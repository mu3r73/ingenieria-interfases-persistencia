"use strict";

window.onload = function init() {
  registerEvents();
};

function registerEvents() {
  document.getElementById("btn").addEventListener("click", agregarRow);
};

function agregarRow() {
  var tbl = document.getElementById("t1");
  
  var tr = tbl.insertRow(-1);
  var td1 = tr.insertCell(0);
  var td2 = tr.insertCell(1);
  var td3 = tr.insertCell(2);

  td1.innerHTML = document.getElementById("ieq").value;
  td2.innerHTML = document.getElementById("ipts").value;
  td3.innerHTML = document.getElementById("ipj").value;
};
