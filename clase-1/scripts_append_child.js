"use strict";

window.onload = function init() {
  registerEvents();
};

function registerEvents() {
  document.getElementById("btn").addEventListener("click", agregarRow);
};

function agregarRow() {
  var tr = document.createElement("tr");
  var td1 = document.createElement("td");
  var td2 = document.createElement("td");
  var td3 = document.createElement("td");

  td1.appendChild(document.createTextNode(document.getElementById("ieq").value));
  td2.appendChild(document.createTextNode(document.getElementById("ipts").value));
  td3.appendChild(document.createTextNode(document.getElementById("ipj").value));

  tr.appendChild(td1);
  tr.appendChild(td2);
  tr.appendChild(td3);

  document.getElementById("t1").appendChild(tr);
};
