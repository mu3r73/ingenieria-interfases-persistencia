var visibles = {
  "ana": true,
  "beto": true,
  "clara": false,
}

fix_btns("ana")
fix_btns("beto")
fix_btns("clara")

function set_estado(nombre, tf) {
  visibles[nombre] = tf
  show_ana_beto_clara()
  fix_btns(nombre)
}

function fix_btns(nombre) {
  document.getElementById("btn_entra_" + nombre).disabled = visibles[nombre]
  document.getElementById("btn_sale_" + nombre).disabled = !visibles[nombre]
}

function show_ana_beto_clara() {
  var txt = ""
  for (nombre in visibles) {
    txt += visibles[nombre] ? (capitalize(nombre) + " ") : ""
  }
  document.getElementById("ps").innerHTML = txt
}

function capitalize(txt) {
  return txt.charAt(0).toUpperCase() + txt.slice(1)
}
