class Numero {
  constructor(num) {
    this.num = num
  }

  esPleno(n) {
    return this.num === n
  }

  esNegro() {
    const negros = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35]
    return negros.includes(this.num)
  }

  esRojo() {
    return (this.num !== 0) && !this.esNegro()
  }

  esImpar() {
    return this.num % 2 === 1
  }

  esPar() {
    return (this.num !== 0) && !this.esImpar()
  }

  esFalta() {
    return (1 <= this.num) && (this.num <= 18)
  }

  esPasa() {
    return (this.num !== 0) && !this.esFalta()
  }

  esCalle(n) {
    return (3 * (n-1) + 1 <= this.num) && (this.num <= 3 * n)
  }

  esSeisena(n) {
    return (6 * (n-1) + 1 <= this.num) && (this.num <= 6 * n)
  }

  esDocena(n) {
    return (12 * (n-1) + 1 <= this.num) && (this.num <= 12 * n)
  }

  esColumna(n) {
    // n es siempre [1 .. 3]
    return Numero.getColumna(this.num) === Numero.getColumna(n)
  }

  static sonAdyacentes(n1, n2) {
    // n2 ¿es adyacente a n1?
    if ((n1 === 0) || (n2 === 0)) {
      return false
    } else {
      let n1Row = Numero.getFila(n1)
      let n1Col = Numero.getColumna(n1)
      let n2Row = Numero.getFila(n2)
      let n2Col = Numero.getColumna(n2)
      return (n1Col - 1 <= n2Col) && (n2Col <= n1Col + 1)
          && (n1Row - 1 <= n2Row) && (n2Row <= n1Row + 1)
    }
  }

  static getFila(n) {
    return (n === 0) ? 0 : (Math.floor((n - 1) / 3) + 1)
  }

  static getColumna(n) {
    return (n === 0) ? 0 : ((n % 3) || 3)
  }

  static sonAdyacentesEnDiagonal(n1, n2) {
    // n2 ¿es adyacente en diagonal a n1?
    if ((n1 == 0) || (n2 == 0)) {
      return false
    } else {
      let n1Row = Numero.getFila(n1)
      let n1Col = Numero.getColumna(n1)
      let n2Row = Numero.getFila(n2)
      let n2Col = Numero.getColumna(n2)
      return ((n2Row === n1Row - 1) && (n2Col === n1Col - 1))
          || ((n2Row === n1Row - 1) && (n2Col === n1Col + 1))
          || ((n2Row === n1Row + 1) && (n2Col === n1Col - 1))
          || ((n2Row === n1Row + 1) && (n2Col === n1Col + 1))
    }
  }
}
