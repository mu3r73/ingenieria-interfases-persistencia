window.onload = () => {
  addListeners()
}

function addListeners() {
  addListenersToApostadores()
  addListenersToFichas()
  addListenerToCantFichas()
  addListenersToNumeros()
  addListenersToApuestas3()
  addListenersToApuestas6()
  addListenersToApuestas12()
  addListenersToApuestas18()
  addListenersToAcciones()
  addListenerToPonerBola()
}

function addListenersToApostadores() {
  for (let btnApo of document.querySelectorAll('.apostador')) {
    btnApo.onchange = () => {
      setApostador(btnApo)
    }
  }
}

function addListenersToFichas() {
  for (let btnFicha of document.querySelectorAll('.ficha')) {
    btnFicha.onchange = () => {
      setFicha(btnFicha)
    }
  }
}

function addListenerToCantFichas() {
  let input = document.querySelector("#cant_fichas")
  input.onchange = () => {
    setCantFichas(input.value)
  }
}

function addListenersToNumeros() {
  for (let btnNum of document.querySelectorAll('.numero')) {
    btnNum.onclick = () => {
      setApuesta(btnNum, setApuestaNumero)
    }
  }
}

function addListenersToApuestas3() {
  for (let btn of document.querySelectorAll('.calle')) {
    btn.onclick = () => {
      setApuesta(btn, setApuesta3o6o12, 'Apuesta3Numeros')
    }
  }
}

function addListenersToApuestas6() {
  for (let btn of document.querySelectorAll('.seisena')) {
    btn.onclick = () => {
      setApuesta(btn, setApuesta3o6o12, 'Apuesta6Numeros')
    }
  }
}

function addListenersToApuestas12() {
  const apuestas = {
    'docena': 'ApuestaDocena',
    'columna': 'ApuestaColumna',
  }
  for (let apu in apuestas) {
    for (let btn of document.querySelectorAll(`.${apu}`)) {
      btn.onclick = () => {
        setApuesta(btn, setApuesta3o6o12, apuestas[apu])
      }
    }
  }
}

function addListenersToApuestas18() {
  const apuestas = {
    'rojo': 'ApuestaRojo',
    'negro': 'ApuestaNegro',
    'par': 'ApuestaPar',
    'impar': 'ApuestaImpar',
    'falta': 'ApuestaFalta',
    'pasa': 'ApuestaPasa',
  }
  for (let apu in apuestas) {
    let btn = document.querySelector(`#btn_${apu}`)
    btn.onclick = () => {
      setApuesta(btn, setApuesta18, apuestas[apu])
    }
  }
}

function addListenersToAcciones() {
  document.querySelector("#btn_apostar").onclick = () => {
    aceptarApuesta()
  }
  document.querySelector("#btn_cancelar").onclick = () => {
    cancelarApuesta()
  }
  document.querySelector("#btn_cargar_debug").onclick = () => {
    cargarApuestasDebug()
  }
  document.querySelector("#btn_tirar").onclick = () => {
    tirarBola()
  }
  document.querySelector("#btn_nuevo").onclick = () => {
    nuevaRonda()
  }
}

function addListenerToPonerBola() {
  let input = document.querySelector("#input_bola")
  input.onchange = () => {
    setBola(input.value)
  }
}
