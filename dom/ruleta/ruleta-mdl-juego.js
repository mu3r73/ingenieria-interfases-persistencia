class Juego {
  constructor() {
    this.numero = undefined
    this.mesaCerrada = false
    this.apuestas = []
  }

  empezarNuevaApuesta() {
    this.checkMesaCerrada()
    this.apuesta = new Apuesta()
  }

  setApostador(num) {
    this.checkMesaCerrada()
    this.apuesta.apostador = num
  }

  setFicha(num) {
    this.checkMesaCerrada()
    this.apuesta.setFicha(num)
  }

  setCantFichas(num) {
    this.checkMesaCerrada()
    this.apuesta.setCantFichas(num)
  }

  asignarApuesta(constrFn, otrosCampos = {}) {
    this.apuesta = Object.assign(new constrFn(), this.apuesta, otrosCampos)
  }

  aceptarApuesta() {
    this.checkEsPosibleApostar()
    this.apuestas.push(this.apuesta)
  }

  cargarApuestaDebug(apuesta) {
    this.checkMesaCerrada()
    this.apuesta = apuesta
  }

  tirarBola() {
    this.checkEsPosibleTirarBola()
    this.mesaCerrada = true
    this.numero = new Numero(Math.floor(Math.random() * 37))
  }

  setBolaDebug(num) {
    this.numero = new Numero(num)
  }

  cuantoAposto(index) {
    return this.apuestas[index].monto
  }

  cuantoGanoLaApuesta(index) {
    return this.apuestas[index].cuantoGano(this.numero)
  }

  checkEsPosibleApostar() {
    this.checkMesaCerrada()
    this.checkApostadorSeleccionado()
    this.checkMontoSeleccionado()
    this.checkApuestaSeleccionada()
  }

  checkEsPosibleElegirApuesta() {
    this.checkMesaCerrada()
    this.checkApostadorSeleccionado()
    this.checkMontoSeleccionado()
  }

  checkEsPosibleTirarBola() {
    this.checkMesaCerrada()
    this.checkHayApuestas()
  }

  checkMesaCerrada() {
    if (this.mesaCerrada) {
      throw new Error("mesa cerrada")
    }
  }

  checkHayApuestas() {
    if (this.apuestas.length == 0) {
      throw new Error("esperar apuestas antes de tirar")
    }
  }

  checkApostadorSeleccionado() {
    if (!this.apuesta.apostador) {
      throw new Error("falta elegir apostador")
    }
  }

  checkMontoSeleccionado() {
    if (!this.apuesta.monto) {
      throw new Error("falta elegir monto")
    }
  }

  checkApuestaSeleccionada() {
    this.apuesta.cerrar()
  }

}
