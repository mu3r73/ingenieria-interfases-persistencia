class Apuesta extends Object {
  constructor(apostador, monto) {
    super()
    this.apostador = apostador
    this.monto = monto
  }

  setFicha(ficha) {
    this.ficha = ficha
    this.actualizarMonto()
  }

  setCantFichas(num) {
    this.cant = num
    this.actualizarMonto()
  }

  actualizarMonto() {
    this.monto = this.ficha * (this.cant || 1)
  }

  cerrar() {
    throw new Error("apuesta incompleta")
  }

  tipo() {
    return ''
  }

  toString() {
    return `apostador: ${this.apostador} - monto: ${this.monto} - ${this.tipo()}`
  }
}

class Apuesta1Numero extends Apuesta {
  constructor(apostador, monto, nums) {
    super(apostador, monto)
    this.nums = [nums]
  }

  cerrar() {
    // nada
  }

  esAdyacente(num) {
    return !this.nums.includes(num)
        && Numero.sonAdyacentes(this.nums[0], num)
        && !(this.nums[1] && !Numero.sonAdyacentes(this.nums[1], num))
  }

  esDiagonal(num) {
    return !this.nums.includes(num)
        && Numero.sonAdyacentesEnDiagonal(this.nums[0], num)
  }

  esGanadora(numSorteado) {
    return numSorteado.esPleno(this.nums[0])
  }

  cuantoGano(numSorteado) {
    if (this.esGanadora(numSorteado)) {
      return 36 * this.monto
    } else {
      return 0
    }
  }

  numsAsString() {
    return this.nums.join(", ")
  }

  tipo() {
    return `pleno: ${this.numsAsString()}`
  }
}

class Apuesta2Numeros extends Apuesta1Numero {
  constructor(apostador, monto, nums) {
    super(apostador, monto, nums)
  }

  esCompleta() {
    return this.nums.length === 2
  }

  completar() {
    // nada
  }

  esGanadora(numSorteado) {
    return this.nums.includes(numSorteado.num)
  }

  cuantoGano(numSorteado) {
    if (this.esGanadora(numSorteado)) {
      return 18 * this.monto
    } else {
      return 0
    }
  }

  tipo() {
    return `caballo: ${this.numsAsString()}`
  }
}

class Apuesta4Numeros extends Apuesta2Numeros {
  constructor(apostador, monto, nums) {
    super(apostador, monto, nums)
  }

  esCompleta() {
    return this.nums.length === 4
  }

  completar() {
    if (this.esCompleta()) {
      return
    }

    // 2 números (en diagonal) => completar hasta 4
    let minmax = this.hallarMinYMax()
    let min = minmax[0]
    let max = minmax[1]
    for (let num = min - 1; num <= max + 1; num++) {
      if (!this.nums.includes(num)
        && (num > 0) && (num < 37)
        && this.esAdyacente(num)) {
        this.nums.push(num)
      }
    }
  }

  hallarMinYMax() {
    return this.nums.reduce(
      (minmax, x) => {
        if (x < minmax[0]) {
          minmax[0] = x
        }
        if (x > minmax[1]) {
          minmax[1] = x
        }
        return minmax
      },
      [Number.MAX_VALUE, Number.MIN_VALUE]
    )
  }

  esGanadora(numSorteado) {
    return this.nums.includes(numSorteado.num)
  }

  cuantoGano(numSorteado) {
    if (this.esGanadora(numSorteado)) {
      return 9 * this.monto
    } else {
      return 0
    }
  }

  tipo() {
    return `esquina: ${this.numsAsString()}`
  }
}

class Apuesta3Numeros extends Apuesta {
  constructor(apostador, monto, num) {
    super(apostador, monto)
    this.num =  num
  }

  cerrar() {
    // nada
  }

  esGanadora(numSorteado) {
    return numSorteado.esCalle(this.num)
  }

  cuantoGano(numSorteado) {
    if (this.esGanadora(numSorteado)) {
      return 12 * this.monto
    } else {
      return 0
    }
  }

  tipo() {
    return `calle: ${this.num}`
  }
}

class Apuesta6Numeros extends Apuesta3Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esSeisena(this.num)
  }

  cuantoGano(numSorteado) {
    return super.cuantoGano(numSorteado) / 2
  }

  tipo() {
    return `seisena: ${this.num}`
  }
}

class Apuesta12Numeros extends Apuesta6Numeros {
  cuantoGano(numSorteado) {
    return super.cuantoGano(numSorteado) / 2
  }
}

class ApuestaDocena extends Apuesta12Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esDocena(this.num)
  }

  tipo() {
    return `docena: ${this.num}`
  }
}

class ApuestaColumna extends Apuesta12Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esColumna(this.num)
  }

  tipo() {
    return `columna: ${this.num}`
  }
}

class Apuesta18Numeros extends Apuesta {
  cerrar() {
    // nada
  }

  cuantoGano(numSorteado) {
    if (this.esGanadora(numSorteado)) {
      return 2 * this.monto
    } else {
      return 0
    }
  }
}

class ApuestaNegro extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esNegro()
  }

  tipo() {
    return "color: negro"
  }
}

class ApuestaRojo extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esRojo()
  }

  tipo() {
    return "color: rojo"
  }
}

class ApuestaPar extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esPar()
  }

  tipo() {
    return "paridad: par"
  }
}

class ApuestaImpar extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esImpar()
  }

  tipo() {
    return "paridad: impar"
  }
}

class ApuestaFalta extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esFalta()
  }

  tipo() {
    return "falta (1-18)"
  }
}

class ApuestaPasa extends Apuesta18Numeros {
  esGanadora(numSorteado) {
    return numSorteado.esPasa()
  }

  tipo() {
    return "pasa (19-36)"
  }
}

const clases = {
  // 1
  Apuesta1Numero,
  // 2
  Apuesta2Numeros,
  // 3
  Apuesta3Numeros,
  // 4
  Apuesta4Numeros,
  // 6
  Apuesta6Numeros,
  // 12
  ApuestaDocena,
  ApuestaColumna,
  // 18
  ApuestaNegro,
  ApuestaRojo,
  ApuestaPar,
  ApuestaImpar,
  ApuestaFalta,
  ApuestaPasa,
}

function getConstructorFn(clsName) {
  return clases[clsName]
}
