let juego = undefined

nuevaRonda()


function nuevaRonda() {
  juego = new Juego()
  juego.empezarNuevaApuesta()
  resetearRonda()
}

function aceptarApuesta() {
  try {
    juego.aceptarApuesta()
    mostrarActualizacionApuestas()
    juego.empezarNuevaApuesta()
    resetearApuesta()
  } catch(e) {
    mostrarError(e.message)
  }
}

function cancelarApuesta() {
  try {
    juego.empezarNuevaApuesta()
    resetearApuesta()
  } catch(e) {
    mostrarError(e.message)
  }
}

function cargarApuestasDebug() {
  cargarApuestaDebug(new ApuestaDocena(1, 100, 1))
  cargarApuestaDebug(new ApuestaDocena(2, 100, 2))
  cargarApuestaDebug(new ApuestaDocena(3, 100, 3))
  cargarApuestaDebug(new Apuesta6Numeros(1, 100, 1))
  cargarApuestaDebug(new Apuesta6Numeros(2, 100, 2))
  cargarApuestaDebug(new Apuesta6Numeros(3, 100, 3))
  cargarApuestaDebug(new Apuesta6Numeros(4, 100, 4))
  cargarApuestaDebug(new Apuesta6Numeros(5, 100, 5))
  cargarApuestaDebug(new Apuesta6Numeros(1, 100, 6))
  cargarApuestaDebug(new Apuesta3Numeros(1, 100, 1))
  cargarApuestaDebug(new Apuesta3Numeros(2, 100, 2))
  cargarApuestaDebug(new Apuesta3Numeros(3, 100, 3))
  cargarApuestaDebug(new Apuesta3Numeros(4, 100, 4))
  cargarApuestaDebug(new Apuesta3Numeros(5, 100, 5))
  cargarApuestaDebug(new Apuesta3Numeros(1, 100, 6))
  cargarApuestaDebug(new Apuesta3Numeros(2, 100, 7))
  cargarApuestaDebug(new Apuesta3Numeros(3, 100, 8))
  cargarApuestaDebug(new Apuesta3Numeros(4, 100, 9))
  cargarApuestaDebug(new Apuesta3Numeros(5, 100, 10))
  cargarApuestaDebug(new Apuesta3Numeros(1, 100, 11))
  cargarApuestaDebug(new Apuesta3Numeros(2, 100, 12))
  cargarApuestaDebug(new ApuestaColumna(1, 100, 1))
  cargarApuestaDebug(new ApuestaColumna(2, 100, 2))
  cargarApuestaDebug(new ApuestaColumna(3, 100, 3))
  cargarApuestaDebug(new ApuestaFalta(1, 100))
  cargarApuestaDebug(new ApuestaPasa(2, 100))
  cargarApuestaDebug(new ApuestaPar(3, 100))
  cargarApuestaDebug(new ApuestaImpar(4, 100))
  cargarApuestaDebug(new ApuestaNegro(5, 100))
  cargarApuestaDebug(new ApuestaRojo(1, 100))
  cargarApuestaDebug(new Apuesta1Numero(2, 100, 0))
}

function cargarApuestaDebug(apuesta) {
  juego.cargarApuestaDebug(apuesta)
  mostrarActualizacionApuestas()
  juego.aceptarApuesta()
  juego.empezarNuevaApuesta()
}

function tirarBola() {
  try {
    juego.tirarBola()
    mostrarTirada()
    mostrarResultados()
    deshabilitarBotones(".accion")
  } catch(e) {
    mostrarError(e.message)
  }
}

function setBola(num) {
  try {
    let input = $("#input_bola")
    num = Math.min(Math.max(num, input[0].min), input[0].max)
    input.val(num)
    juego.setBolaDebug(num)
    mostrarTirada()
    mostrarResultados()
  } catch(e) {
    mostrarError(e.message)
  }
}

function resetearRonda() {
  resetearApuesta()
  habilitarBotones(".accion")
  borrarResultados()
  borrarTirada()
}

function resetearApuesta() {
  resetearRadios(".apostador", true)
  resetearRadios(".ficha")
  resetearInput("#cant_fichas", 1)
  resetearBotones(".numero, .calle, .seisena, .docena, .columna, .apuesta18")
}

function borrarResultados() {
  $("#apuestas").find("tr:gt(0)").remove()
  $("#totales").html("")
}

function mostrarTirada() {
  $("#resultado").html(`- cayó en número: <strong>${juego.numero.num}</strong>`)
}

function borrarTirada() {
  $("#resultado").html("")
  $("#input_bola").val("")
}

function mostrarResultados() {
  let total_apostado = 0
  let total_ganado = 0
  let i = 0
  for (let td of $("#apuestas tr:gt(0) td:last-child")) {
    total_apostado += juego.cuantoAposto(i)
    let ganado = juego.cuantoGanoLaApuesta(i)
    total_ganado += ganado
    td.innerHTML = `$${ganado}`
    i++
  }
  $("#totales").html(`
    total apostado: <strong>$${total_apostado}</strong> - 
    total ganado por jugadores: <strong>$${total_ganado}</strong> =>
    balance de la ronda: <strong>$${total_apostado - total_ganado}</strong>
  `)
}

function setApostador(rb) {
  try {
    juego.setApostador(rb.id.replace("apo", ""))
    ocultarApostadoresExcepto(rb)
    console.log(juego.apuesta.toString())
  } catch(e) {
    mostrarError(e.message)
  }
}

function setFicha(rb) {
  try {
    juego.setFicha(rb.id.replace("ficha", ""))
    ocultarFichasExcepto(rb)
    console.log(juego.apuesta.toString())
  } catch(e) {
    mostrarError(e.message)
  }
}

function setCantFichas(cant) {
  try {
    let input = $("#cant_fichas")
    cant = Math.min(Math.max(cant, input[0].min), input[0].max)
    input.val(cant)
    juego.setCantFichas(cant)
    console.log(juego.apuesta.toString())
  } catch(e) {
    mostrarError(e.message)
  }
}

function setApuesta(btn, fnSetApuesta, apuestaClsName = 'Apuesta') {
  try {
    juego.checkEsPosibleElegirApuesta()
    fnSetApuesta(btn, apuestaClsName)
    ocultarBotonesDeApuestaExcepto(btn)
    console.log(juego.apuesta.toString())
  } catch (e) {
    mostrarError(e.message)
  }
}

function setApuestaNumero(btn) {
  let num = Number.parseInt(btn.textContent)
  let constrFn = undefined
  if (juego.apuesta.esAdyacente) {
    if (juego.apuesta.esDiagonal(num)) {
      constrFn = getConstructorFn('Apuesta4Numeros')
    } else if (juego.apuesta.esAdyacente(num)) {
      constrFn = getConstructorFn('Apuesta2Numeros')
    }
    juego.apuesta.nums.push(num)
    juego.asignarApuesta(constrFn, {nums: juego.apuesta.nums})
    juego.apuesta.completar()
  } else {
    constrFn = getConstructorFn('Apuesta1Numero')
    juego.asignarApuesta(constrFn, {nums: [num]})
  }
}

function setApuesta18(btn, clsName) {
  let constrFn = getConstructorFn(clsName)
  juego.asignarApuesta(constrFn)
}

function setApuesta3o6o12(btn, clsName) {
  let constrFn = getConstructorFn(clsName)
  let num = Number.parseInt(btn.textContent.replace(/[^0-9]/g, ''))
  juego.asignarApuesta(constrFn, {num: num})
}

function mostrarActualizacionApuestas() {
  $("#apuestas").find('tbody')
    .append($('<tr>')
      .append($(`<td>${juego.apuesta.apostador}</td>`))
      .append($(`<td>$${juego.apuesta.monto}</td>`))
      .append($(`<td>${juego.apuesta.tipo()}</td>`))
      .append($(`<td></td>`))
    )
}

function resetearInput(id, val) {
  $(id).val(val)
}

function resetearRadios(cls, unSoloUso = false) {
  for (let rb of $(cls)) {
    if ((rb.checked)) {
      deselectRadio(rb)
      if (unSoloUso) {
        ocultarElemento(rb.closest("label"))
      }
    }
    mostrarElemento(rb.closest("label"))
  }
}

function deshabilitarBotones(cls) {
  $(cls).addClass("disabled")
}

function habilitarBotones(cls) {
  $(cls).removeClass("disabled")
}

function resetearBotones(cls) {
  for (let btn of $(cls)) {
    mostrarElemento(btn)
  }
}

function ocultarApostadoresExcepto(rb) {
  ocultarRadiosExcepto(".apostador", rb)
}

function ocultarFichasExcepto(rb) {
  ocultarRadiosExcepto(".ficha", rb)
}

function ocultarRadiosExcepto(cls, rb) {
  for (let r of $(`${cls}:not(#${rb.id})`)) {
    ocultarElemento(r.closest("label"))
  }
}

function ocultarBotonesDeApuestaExcepto(btn) {
  for (let b of $(".calle, .seisena, .docena, .columna, .apuesta18")) {
    if (b.id !== btn.id) {
      ocultarElemento(b)
    }
  }
  for (let b of $(".numero")) {
    let num = Number.parseInt(b.textContent)
    if (!((juego.apuesta.nums && juego.apuesta.nums.includes(num))
         || (!(juego.apuesta.esCompleta && juego.apuesta.esCompleta())
             && juego.apuesta.esAdyacente && juego.apuesta.esAdyacente(num)))) {
      ocultarElemento(b)
    }
  }
}

function ocultarElemento(e) {
  e.style.visibility = "hidden"
}

function mostrarElemento(e) {
  e.style.visibility = "visible"
}

function deselectRadio(rb) {
  rb.checked = false
  $(rb.labels).removeClass("active")
}

function isPressed(btn) {
  return btn.hasClass("active")  // bootstrap
}

function toggleButton(btn) {
  btn.toggleClass("active")  // bootstrap
}

function mostrarError(msg) {
  if (juego.mesaCerrada) {
    console.log(msg)
  } else {
    alert(msg)
  }
}
