window.onload = () => {
  addListeners()
}

function addListeners() {
  addListenersToApostadores()
  addListenersToFichas()
  addListenerToCantFichas()
  addListenersToNumeros()
  addListenersToApuestas3()
  addListenersToApuestas6()
  addListenersToApuestas12()
  addListenersToApuestas18()
  addListenersToAcciones()
  addListenerToPonerBola()
}

function addListenersToApostadores() {
  $(".apostador").change((event) => {
    setApostador(event.target)
  })
}

function addListenersToFichas() {
  $(".ficha").change((event) => {
    setFicha(event.target)
  })
}

function addListenerToCantFichas() {
  $("#cant_fichas").change((event) => {
    setCantFichas(event.target.value)
  })
}

function addListenersToNumeros() {
  $(".numero").click((event) => {
    setApuesta(event.target, setApuestaNumero)
  })
}

function addListenersToApuestas3() {
  $(".calle").click((event) => {
    setApuesta(event.target, setApuesta3o6o12, 'Apuesta3Numeros')
  })
}

function addListenersToApuestas6() {
  $(".seisena").click((event) => {
    setApuesta(event.target, setApuesta3o6o12, 'Apuesta6Numeros')
  })
}

function addListenersToApuestas12() {
  const tipos = {
    'docena': 'ApuestaDocena',
    'columna': 'ApuestaColumna',
  }
  for (let tipo in tipos) {
    let fn = tipos[tipo]
    let t = `.${tipo}`
    $(t).click((event) => {
      setApuesta(event.target, setApuesta3o6o12, fn)
    })
  }
}

function addListenersToApuestas18() {
  const btns = {
    'btn_rojo': 'ApuestaRojo',
    'btn_negro': 'ApuestaNegro',
    'btn_par': 'ApuestaPar',
    'btn_impar': 'ApuestaImpar',
    'btn_falta': 'ApuestaFalta',
    'btn_pasa': 'ApuestaPasa',
  }
  for (let btn in btns) {
    let fn = btns[btn]
    let b = `#${btn}`
    $(b).click((event) => {
      setApuesta(event.target, setApuesta18, fn)
    })
  }
}

function addListenersToAcciones() {
  $("#btn_apostar").click(() => {
    aceptarApuesta()
  })
  $("#btn_cancelar").click(() => {
    cancelarApuesta()
  })
  $("#btn_cargar_debug").click(() => {
    cargarApuestasDebug()
  })
  $("#btn_tirar").click(() => {
      tirarBola()
    })
  $("#btn_nuevo").click(() => {
    nuevaRonda()
  })
}

function addListenerToPonerBola() {
  $("#input_bola").change((event) => {
    setBola(event.target.value)
  })
}
