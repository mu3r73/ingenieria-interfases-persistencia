import urllib.request


def download_page_to_file(page):
  url = f'http://api.worldbank.org/v2/es/indicators?format=json&page={page}'
  file_name = f'indicators_p{page}.json'
  print('downloading', url, '...')
  with urllib.request.urlopen(url) as response:
    file = open(file_name, "w")
    file.write(response.read().decode('utf-8'))
    file.close()


def main():
  for i in range(1, 343):
    print(f'i: {i}')
    download_page_to_file(i)


if __name__ == "__main__":
  main()
