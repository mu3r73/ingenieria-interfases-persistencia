let request = window.superagent

let paisSeleccionado


window.onload = () => {
  initRegiones()
  initIndicadores()
  addRegionesListener()
  addPaisesListener()
  addIndicadoresListener()
}

function initRegiones() {
  const regiones = {
    SSF: "África al sur del Sahara",
    NAC: "América del Norte",
    LCN: "América Latina y el Caribe",
    SAS: "Asia meridional",
    EAS: "Asia oriental y el Pacífico",
    ECS: "Europa y Asia central",
    MEA: "Oriente Medio y Norte de África",
  }
  let selRegiones = document.getElementById("regiones")

  agregarOpcion0(selRegiones, "elegir región")

  for (codRegion in regiones) {
    let opcion = document.createElement("option")
    opcion.value = codRegion
    opcion.text = regiones[codRegion]
    selRegiones.add(opcion, null)
  }
}

function initIndicadores() {
  const indicadores = {
    "SE.XPD.TOTL.GB.ZS": "Gasto público en educación, total (% del gasto del gobierno)",
    "SE.XPD.TOTL.GD.ZS": "Gasto público en educación, total (% del PIB)",
    "SE.PRM.CMPT.ZS": "Tasa de finalización de la educación de nivel primario, total (% del grupo etario correspondiente)",
    "2.0.cov.FPS": "Tasa de Cobertura: Finalización de la Educación Primaria",
    "SE.ADT.LITR.ZS": "Tasa de alfabetización, total de adultos (% de personas de 15 años o más)",
    "SE.ADT.1524.LT.ZS": "Tasa de alfabetización, total de jóvenes (% de personas entre 15 y 24 años)",

    "EG.ELC.ACCS.ZS": "Acceso a la electricidad (% de población)",

    "2.0.cov.Int": "Tasa de Cobertura: Internet",
    "2.0.cov.Ele": "Tasa de Cobertura: Electricidad",
    "2.0.cov.Cel": "Tasa de Cobertura: Teléfono móvil",
    "2.0.cov.Wat": "Tasa de Cobertura: Agua",
    "2.0.cov.San": "Tasa de Cobertura: Saneamiento",

    "1.0.HCount.Vul4to10": "Tasa de incidencia de población Vulnerable ($4-10 al día)",
    "1.2.HCount.Poor4uds": "Tasa de Incidencia de la Pobreza ($4 al día)-Urbano",
    "1.0.HCount.Mid10to50": "Tasa de Incidencia de la Clase Media ($10-50 al día)",
    
    "9.0.Labor.All": "Tasa de Participación en la Fuerza Laboral (%)",
    "SL.UEM.TOTL.ZS": "Desempleo, total (% de la población activa total) (estimación modelado OIT)",

    "SI.DST.10TH.10": "Participación en el ingreso del 10% mejor remunerado de la población",
    "SI.DST.FRST.10": "Participación en el ingreso del 10% peor remunerado de la población",

    "PA.NUS.FCRF": "Tasa de cambio oficial (UMN por US$, promedio para un período)",
    
    "GC.DOD.TOTL.GD.ZS": "Deuda del gobierno central, total (% del PIB)",
    "NE.EXP.GNFS.ZS": "Exportaciones de bienes y servicios (% del PIB)",
    "DT.DOD.DECT.GN.ZS": "Deuda externa acumulada (% del INB)",
    "NY.GDP.MKTP.KD.ZG": "Crecimiento del PIB (% anual)",
    "NE.IMP.GNFS.ZS": "Importaciones de bienes y servicios (% del PIB)",
    "NV.IND.TOTL.ZS": "Industria, valor agregado (% del PIB)",
    "NY.GDP.DEFL.KD.ZG": "Inflación, índice de deflación del PIB (% anual)",
    "FP.CPI.TOTL.ZG": "Inflación, precios al consumidor (% anual)",
    "GC.REV.XGRT.GD.ZS": "Recaudación, excluidas las donaciones (% del PIB)",
    "DT.DOD.DSTC.IR.ZS": "Deuda a corto plazo (% del total de reservas)",
  }
  let selIndicadores = document.getElementById("indicadores")

  agregarOpcion0(selIndicadores, "elegir indicador")

  for (codIndicador in indicadores) {
    let opcion = document.createElement("option")
    opcion.value = codIndicador
    opcion.text = indicadores[codIndicador]
    selIndicadores.add(opcion, null)
  }
}

function addRegionesListener() {
  let selRegiones = document.getElementById("regiones")

  selRegiones.onchange = (event) => {
    codRegion = event.target.value
    if (codRegion) {
      fetchPaises(codRegion)
    }
  }
}

function addPaisesListener() {
  let selPaises = document.getElementById("paises")

  selPaises.onchange = (event) => {
    paisSeleccionado = event.target.value
    if (paisSeleccionado) {
      console.log(paisSeleccionado)
    }
  }
}

function addIndicadoresListener() {
  let selIndicadores = document.getElementById("indicadores")

  selIndicadores.onchange = (event) => {
    codIndicador = event.target.value
    if (codIndicador && paisSeleccionado) {
      fetchIndicador(paisSeleccionado, codIndicador)
    }
  }
}

function fetchPaises(codRegion) {
  let url = "http://api.worldbank.org/v2/es/countries"
  let query = `region=${codRegion}&format=json`

  request
    .get(url)
    .query(query)
    .then((response) => {
      console.log(`response ok: ${response.ok}`)
      initPaises(response.text)
    })
    .catch((err) => {
      console.log(err)
    })
}

function fetchIndicador(codPais, codIndicador) {
  let url = `http://api.worldbank.org/v2/es/countries/${codPais}/indicators/${codIndicador}`
  let query = "format=json"

  request
    .get(url)
    .query(query)
    .then((response) => {
      console.log(`response ok: ${response.ok}`)
      mostrarIndicador(response.text)
    })
    .catch((err) => {
      console.log(err)
    })
}

function initPaises(jsonText) {
  let jsonObj = JSON.parse(jsonText)
  let selPaises = document.getElementById("paises")

  removeOptions(selPaises)

  agregarOpcion0(selPaises, "elegir país")

  try {
    for (p of jsonObj[1]) {
      if (p.capitalCity) {
        let opcion = document.createElement("option")
        opcion.value = p.id
        opcion.text = p.name
        selPaises.add(opcion, null)
      }
    }
  } catch(err) {
    console.log(err)
    console.log(jsonText)
  }
}

function removeOptions(select) {
  while (select.firstChild) {
    select.removeChild(select.firstChild)
  }
}

function agregarOpcion0(select, txt) {
  let opcion = document.createElement("option")
  opcion.value = ""
  opcion.text = `(${txt})`
  select.add(opcion, null)
}

function mostrarIndicador(jsonText) {
  let jsonObj = JSON.parse(jsonText)
  console.log(jsonObj)
  let tblResultados = document.getElementById('resultados')
  borrarDatos(tblResultados)
  try {
    for (p of jsonObj[1]) {
      if (p.value) {
        agregarDatos(tblResultados, p.date, p.value)
      }
    }
  } catch(err) {
    console.log(err)
    console.log(jsonText)
  }
}

function borrarDatos(tbl) {
  for (let i = tbl.rows.length - 1; i > 0; i--) {
    tbl.deleteRow(i)
  }
}

function agregarDatos(tbl, anho, valor) {
  let tr = tbl.insertRow(-1)
  
  let td1 = tr.insertCell(0)
  td1.innerHTML = anho

  let td2 = tr.insertCell(1)
  td2.innerHTML = valor
}
