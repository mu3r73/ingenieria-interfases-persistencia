var ana = false
var beto = true

fix_btns_ana()
fix_btns_beto()

function set_ana(tf) {
  ana = tf
  show_ana_y_beto()
  fix_btns_ana()
}

function fix_btns_ana() {
  document.getElementById("btn_entra_ana").disabled = ana
  document.getElementById("btn_sale_ana").disabled = !ana
}

function set_beto(tf) {
  beto = tf
  show_ana_y_beto()
  fix_btns_beto()
}

function fix_btns_beto() {
  document.getElementById("btn_entra_beto").disabled = beto
  document.getElementById("btn_sale_beto").disabled = !beto
}

function show_ana_y_beto() {
  document.getElementById("ps").innerHTML = (ana ? "Ana " : "") + (beto ? "Beto" : "")
}
