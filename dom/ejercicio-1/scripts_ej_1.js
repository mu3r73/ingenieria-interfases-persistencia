var nro = 0

function sumar(num) {
  nro += num
  actualizar_nro()
}

function multiplicar(num) {
  nro *= num
  actualizar_nro()
}

function actualizar_nro() {
  document.getElementById("num").innerHTML = nro
}
