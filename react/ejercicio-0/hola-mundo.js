const React = require('react')
const ReactDOM = require('react-dom')

class Saludo extends React.Component {
  render() {
    return (
      <h1>hola Lamadrid {this.props.lama}</h1>
    )
  }
}

ReactDOM.render(
  <Saludo lama="hola" />,
  document.getElementById('react_page')
)
