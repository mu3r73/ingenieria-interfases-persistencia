const React = require('react')
const ReactDOM = require('react-dom')

class Boton extends React.Component {
  constructor() {
    super()
    this.state = {
      mensaje: 'gracias',
    }
  }

  render() {
    console.log(this.state)
    return (
      <button onClick={() => this.update()}>{this.state.mensaje}</button>
      // <button onClick={this.update.bind(this)}>{this.state.mensaje}</button>
    )
  }

  update() {
    this.setState({
      mensaje: 'de nada',
    })
    console.log(this.state)
  }
}

ReactDOM.render(
  <Boton />,
  document.getElementById('react_page')
)
