import React, { Component } from 'react'
import ReactDOM from 'react-dom'


class Contador extends Component {
  constructor() {
    super()
    this.state = {
      num: 0,
    }
  }

  render() {
    return (
      <div>
        <p className='form-control'>{this.state.num}</p>
        <button className='btn btn-default mr-1' onClick={() => this.sum(1)}>+ 1</button>
        <button className='btn btn-default mr-1' onClick={() => this.sum(-1)}>- 1</button>
        <button className='btn btn-default mr-1' onClick={() => this.mul(2)}>x 2</button>
        <button className='btn btn-default mr-1' onClick={() => this.reset()}>reset</button>
      </div>
    )
  }

  reset() {
    this.setState({
      num: 0,
    })
  }

  sum(val) {
    this.setState((prevState) => ({
      num: prevState.num + val,
    }))
  }

  mul(val) {
    this.setState((prevState) => ({
      num: prevState.num * val,
    }))
  }
}

ReactDOM.render(
  <Contador />,
  document.getElementById('reactPage')
)
