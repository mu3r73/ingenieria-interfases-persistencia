/*
base:
https://github.com/obj2-material/javascript-dom/blob/master/react/ventas-aereas/aviones-vuelos-design-2.js
*/

import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import DatosDelAvionElegido from './DatosDelAvionElegido'
import DatosDeLaCiudadElegida from './DatosDeLaCiudadElegida'
import TablaVuelos from './TablaVuelos'

const ventas = require('../dominio')


class InfoVuelosAviones extends Component {
  constructor(props) {
    super(props)
    this.state = {
      avionElegido: ventas.store.avionConNombre("Airbus 330"),
      ciudadElegida: ventas.store.ciudadConNombre("Buenos Aires"),
    }
  }

  render() {
    return (
      <div className="container ml-20 mr-20">
        <TablaVuelos
          mainComponent={this}
        />
        <DatosDelAvionElegido
          mainComponent={this}
        />
        <DatosDeLaCiudadElegida
          mainComponent={this}
        />
      </div>
    )
  }

  avionElegido() {
    return this.state.avionElegido
  }

  elegirAvion(nombre) {
    this.setState({
      avionElegido: ventas.store.avionConNombre(nombre)
    })
  }

  ciudadElegida() {
    return this.state.ciudadElegida
  }

  elegirCiudad(nombre) {
    this.setState({
      ciudadElegida: ventas.store.ciudadConNombre(nombre)
    })
  }
}

export default InfoVuelosAviones
