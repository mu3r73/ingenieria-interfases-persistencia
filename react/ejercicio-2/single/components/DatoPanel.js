import React, { Component } from 'react'


class DatoPanel extends Component {
  render() {
    return (
      <div className='row mb-5'>
        <div className='col-md-3'>
          <strong>{this.label()}</strong>
        </div>
        <div className='col-md-9'>
          {this.valor()}
        </div>
      </div>
    )
  }

  label() {
    return this.props.label
  }

  valor() {
    return this.props.valor
  }
}

export default DatoPanel
