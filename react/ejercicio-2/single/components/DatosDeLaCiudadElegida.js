import React, { Component } from 'react'

import DatoPanel from './DatoPanel'


class DatosDeLaCiudadElegida extends Component {
  render() {
    const ciudadElegida = this.ciudadElegida()
    return (
      <div className='panel panel-info mt-10'>
        <div className='panel-heading'>
          <h4>Ciudad {ciudadElegida.nombre()}</h4>
        </div>
        <div className='panel-body'>
          <DatoPanel
            label={'Cantidad de pasajeros que salieron'}
            valor={ciudadElegida.pasajerosQueSalieron()}
          />
          <DatoPanel
            label={'Cantidad de pasajeros que llegaron'}
            valor={ciudadElegida.pasajerosQueLlegaron()}
          />
        </div>
      </div>
    )
  }

  ciudadElegida() {
    return this.props.mainComponent.ciudadElegida()
  }
}


export default DatosDeLaCiudadElegida
