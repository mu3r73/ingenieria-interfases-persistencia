import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import ventas from '../dominio'


class TablaVuelos extends React.Component {
  render() {
    return (
      <div className="panel panel-success mt-50">
        <div className="panel-heading">
          <h4>Vuelos (de todos los aviones)</h4>
        </div>
        <div className="panel-body">
          <table className="table table-striped">
            <thead>
              <tr>
                {this.renderTitulos()}
              </tr>
            </thead>
            <tbody>
              {this.renderVuelos()}
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  mainComponent() {
    return this.props.mainComponent
  }

  vuelos() {
    return ventas.store.vuelos()
  }

  renderTitulos() {
    const titulos = ["Tipo de vuelo", "Origen", "Destino", "Avión",
                     "Asientos libres", "Precio pasaje", "Política de venta",
                     "Pasajes vendidos", "Importe total vendido"]
    return titulos.map(
      titulo => <th key={titulo}>{titulo}</th>
    )
  }

  renderVuelos() {
    return this.vuelos().map(
      vuelo =>
        <DatosVuelo
          vuelo={vuelo}
          key={vuelo.numero()}
          mainComponent={this.mainComponent()}
        />
    )
  }
}


class DatosVuelo extends React.Component {
  render() {
    const vuelo = this.vuelo()
    return (
      <tr>
        <td>{vuelo.tipoAsString()}</td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirCiudad(vuelo.origen().nombre())}
          >
            {vuelo.origen().nombre()}
          </a>
        </td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirCiudad(vuelo.destino().nombre())}
          >
            {vuelo.destino().nombre()}
          </a>
        </td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirAvion(vuelo.avion().nombre())}
          >
            {vuelo.avion().nombre()}
          </a>
        </td>
        <td>{vuelo.cantidadAsientosLibres()}</td>
        <td>{vuelo.precioPasaje()}</td>
        <td>{vuelo.politicaPrecio()}</td>
        <td>{vuelo.cantidadPasajesEmitidos()}</td>
        <td>{vuelo.importeTotalPasajesEmitidos()}</td>
      </tr>
    )
  }

  vuelo() {
    return this.props.vuelo
  }

  elegirAvion(nombre) {
    this.props.mainComponent.elegirAvion(nombre)
  }

  elegirCiudad(nombre) {
    this.props.mainComponent.elegirCiudad(nombre)
  }
}

export default TablaVuelos
