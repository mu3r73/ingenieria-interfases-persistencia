import React, { Component } from 'react'
import ReactDOM from 'react-dom'


import DatoPanel from './DatoPanel'


class DatosDelAvionElegido extends Component {
  render() {
    const avionElegido = this.avionElegido()
    return (
      <div className='panel panel-info mt-10'>
        <div className='panel-heading'>
          <h4>Avión {avionElegido.nombre()}</h4>
        </div>
        <div className='panel-body'>
          <DatoPanel
            label={'Cantidad de vuelos'}
            valor={avionElegido.cantidadVuelosHechos()}
          />
          <DatoPanel
            label={'Capacidad'}
            valor={avionElegido.cantidadAsientos()}
          />
          <DatoPanel
            label={'Pasajeros transportados'}
            valor={avionElegido.cantidadTotalPasajeros()}
          />
          <DatoPanel
            label={'Porcentaje de ocupación'}
            valor={avionElegido.porcentajeOcupacion()}
          />
          <DatoPanel
            label={'Altura de la cabina'}
            valor={avionElegido.alturaCabina()}
          />
        </div>
      </div>
    )
  }

  avionElegido() {
    return this.props.mainComponent.avionElegido()
  }
}

export default DatosDelAvionElegido
