/*
base:
https://github.com/obj2-material/javascript-dom/blob/master/react/ventas-aereas/aviones-vuelos-design.js
*/
import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import InfoVuelosAviones from './components/InfoVuelosAviones'


ReactDOM.render(
    <InfoVuelosAviones />,
    document.getElementById('reactPage')
);
