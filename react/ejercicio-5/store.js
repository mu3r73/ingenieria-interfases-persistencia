import Golondrina from './golondrina'

class Store {
  constructor() {
    this._golondrinas = []
  }

  agregar(golondrina) {
    this._golondrinas.push(golondrina)
  }

  golondrinas() {
    return this._golondrinas
  }
}

const store = new Store()
store.agregar(new Golondrina('Sofía', 60))
store.agregar(new Golondrina('Bakelita', 20))
store.agregar(new Golondrina('Takumi', 40))
store.agregar(new Golondrina('Rosa', 80))

export default store
