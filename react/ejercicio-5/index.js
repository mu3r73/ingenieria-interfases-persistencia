import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import ListaGolondrinas from './components/ListaGolondrinas'
import AgregarGolondrina from './components/AgregarGolondrina'

import Golondrina from './golondrina'
import store from './store'


class App extends Component {
  constructor() {
    super()
    this.state = {
      store,
      comer: [],
      volar: [],
      ultimaOperacion: '',
      showAgregar: false,
    }
  }

  golondrinas() {
    return this.state.store.golondrinas()
  }

  ultimaOperacion() {
    return this.state.ultimaOperacion
  }

  hayUltimaOperacion() {
    return this.state.ultimaOperacion.length !== 0
  }

  setUltimaOperacion(ultimaOperacion) {
    this.setState({
      ultimaOperacion,
    })
  }

  pendientes(accion) {
    // accion: {'comer'|'volar'}
    return this.state[accion]
  }

  hayPendientes() {
    return this.state.comer.length !== 0 || this.state.volar.length !== 0
  }

  comer(golondrina, gramos) {
    golondrina.comer(gramos)
    this.setUltimaOperacion(`${golondrina.nombre()} comió ${gramos} g`)
  }

  volar(golondrina, km) {
    golondrina.volar(km)
    this.setUltimaOperacion(`${golondrina.nombre()} voló ${km} km`)
  }

  accion(golondrina, accion, valor) {
    switch (accion) {
      case 'comer':
        this.comer(golondrina, valor)
        break
      case 'volar':
        this.volar(golondrina, valor)
        break
      default:
    }
  }

  addToAcciones(golondrina, accion) {
    if (this.state[accion].indexOf(golondrina) !== -1) {
      return
    }
    this.setState({
      [accion]: [...this.state[accion], golondrina],
    })
  }

  removeFromAcciones(golondrina, accion) {
    const newAcciones = this.state[accion]
    newAcciones.splice(newAcciones.indexOf(golondrina), 1)
    this.setState({
      [accion]: newAcciones,
    })
  }


  showAgregarGolondrina() {
    this.setState({
      showAgregar: true,
    })
  }

  hideAgregarGolondrina() {
    this.setState({
      showAgregar: false,
    })
  }

  agregarGolondrina(nombre, energia) {
    const golondrina = new Golondrina(nombre, Number(energia))
    this.state.store.agregar(golondrina)
    this.setState({
      golondrinas: this.state.store.golondrinas(),
      ultimaOperacion: `se agregó la golondrina ${golondrina.nombre()}`,
    })
  }

  render() {
    return (
      <div className='container-fluid'>
        {this.renderCurrentComponent()}
      </div>
    )
  }

  renderCurrentComponent() {
    if (this.state.showAgregar) {
      return <AgregarGolondrina mainComponent={this} />
    } else {
      return <ListaGolondrinas mainComponent={this} />
    }
  }
}


ReactDOM.render(
  <App />,
  document.getElementById('reactPage')
)
