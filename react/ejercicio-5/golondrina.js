class Golondrina {
  constructor(nombre, energia) {
    this._nombre = nombre
    this._energia = energia
  }

  nombre() {
    return this._nombre
  }
  
  energia() {
    return this._energia
  }
  
  comer(gramos) {
    this._energia += gramos * 4
  }
  
  volar(kms) {
    this._energia -= kms + 10
  }

  estaFeliz() {
    return (this._energia >= 50) && (this._energia <= 120)
  }
}

export default Golondrina
