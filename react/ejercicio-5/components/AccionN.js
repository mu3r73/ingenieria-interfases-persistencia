import React, { Component } from 'react'


class AccionN extends Component {
  constructor(props, accion, unidad, descripcion) {
    super(props)
    this.state = {
      accion,
      unidad,
      valor: 0,
      descripcion,
      errores: [],
    }
  }

  render() {
    return (
      <div className='form-group'>
        <div className='col-8'>
          <label className='form-label mr-1'>
            {this.state.descripcion} a {this.props.golondrina.nombre()}
            <input className='form-control ml-1 mr-1'
              type='number'
              onChange={
                (event)=>this.setState({
                  valor: Number(event.target.value)
                })
              }
            />
            {this.state.unidad}
          </label>
        </div>

        <button className='btn btn-primary mr-1'
          onClick={() => this.aceptar()}
        >
          confirmar
        </button>
        
        <button className='btn btn-secondary mr-1'
          onClick={() => this.cancelar()}
        >
          cancelar
        </button>
        {this.state.errores && this.renderErrores()}
      </div>
    )
  }

  renderErrores() {
    return (
      <div className='text-danger'>
        {this.state.errores.map(e =>
          <small>{e}</small>
        )}
      </div>
    )
  }

  validarValor() {
    const num = Number(this.state.valor)
    const errores = []
    if (!Number.isFinite(num)) {
      errores.push(`${this.state.unidad} debe ser un número finito`)
    }
    if (num <= 0) {
      errores.push(`${this.state.unidad} debe ser positivo`)
    }
    return errores
  }

  aceptar() {
    const errores = this.validarValor()
    if (!errores.length) {
      this.props.mainComponent.accion(this.props.golondrina, this.state.accion, this.state.valor)
      this.props.mainComponent.removeFromAcciones(this.props.golondrina, this.state.accion)
    } else {
      this.setState({
        errores,
      })
    }
  }

  cancelar() {
    this.props.mainComponent.removeFromAcciones(this.props.golondrina, this.state.accion)
  }
}

export default AccionN
