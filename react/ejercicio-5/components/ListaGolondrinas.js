import React, { Component } from 'react'

import Golondrinas from './Golondrinas'
import OperacionesPendientes from './OperacionesPendientes'
import UltimaOperacion from './UltimaOperacion'


class ListaGolondrinas extends Component {
  render() {
    return (
      <div className='mt-4'>
        <Golondrinas mainComponent={this.props.mainComponent} />
        {this.props.mainComponent.hayUltimaOperacion() && <UltimaOperacion mainComponent={this.props.mainComponent} />}
        {this.props.mainComponent.hayPendientes() && <OperacionesPendientes mainComponent={this.props.mainComponent} />}
      </div>
    )
  }
}

export default ListaGolondrinas
