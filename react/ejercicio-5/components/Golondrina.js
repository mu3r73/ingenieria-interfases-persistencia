import React, { Component } from 'react'


class Golondrina extends Component {
  render() {
    const g = this.props.golondrina
    return (
      <li className='row mb-1'>
        <div className='col-3'>
          {g.nombre()}:
        </div>
        <div className='col-2 text-right'>
          {g.energia()}
        </div>
        <h5 className='col-1'>
          {g.estaFeliz() ? '😃' : '😐'}
        </h5>

        <button className='btn btn-secondary btn-sm mr-1'
          onClick={() => this.comer(10)}
        >
          comer 10
        </button>
        
        <button className='btn btn-secondary btn-sm mr-1'
          onClick={() => this.volar(10)}
        >
          volar 10
        </button>

        <button className='btn btn-secondary btn-sm mr-1'
          onClick={() => this.addToComer()}
        >
          comer n
        </button>

        <button className='btn btn-secondary btn-sm mr-1'
          onClick={() => this.addToVolar()}
        >
          volar n
        </button>
      </li>
    )
  }

  comer(gramos) {
    this.props.mainComponent.comer(this.props.golondrina, gramos)
  }

  volar(km) {
    this.props.mainComponent.volar(this.props.golondrina, km)
  }

  addToComer() {
    this.props.mainComponent.addToAcciones(this.props.golondrina, 'comer')
  }

  addToVolar() {
    this.props.mainComponent.addToAcciones(this.props.golondrina, 'volar')
  }
}

export default Golondrina
