import React, { Component } from 'react'

import AccionN from './AccionN'


class OperacionesPendientes extends Component {
  render() {
    return (
      <div className='row mt-4'>
        <div className='col-10'>
          <div className='card text-white bg-info'>
            <div className='card-header'>
              <span className='lead'>operaciones pendientes</span>
            </div>
            <div className='card-body text-dark bg-light'>
              <div className='row'>
                <div className='form-inline'>
                  {this.renderComerN()}
                  {this.renderVolarN()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderComerN() {
    return (
      <div>
        {
          this.props.mainComponent.pendientes('comer').map(g =>
            <ComerN golondrina={g} mainComponent={this.props.mainComponent} key={g.nombre()} />
          )
        }
      </div>
    )
  }

  renderVolarN() {
    return (
      <div>
        {
          this.props.mainComponent.pendientes('volar').map(g =>
            <VolarN golondrina={g} mainComponent={this.props.mainComponent} key={g.nombre()} />
          )
        }
      </div>
    )
  }
}


class ComerN extends AccionN {
  constructor(props) {
    super(props, 'comer', 'gramos', 'darle de comer')
  }
}


class VolarN extends AccionN {
  constructor(props) {
    super(props, 'volar', 'km', 'hacer volar')
  }
}

export default OperacionesPendientes
