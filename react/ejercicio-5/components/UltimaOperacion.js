import React, { Component } from 'react'


class UltimaOperacion extends Component {
  render() {
    return (
      <div className='row mt-4'>
        <div className='col-10'>
          <div className='card bg-secondary'>
            <div className='card-header text-white'>
              <span className='lead'>última operación</span>
            </div>
            <div className='card-body text-dark bg-white'>
              {this.props.mainComponent.ultimaOperacion()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UltimaOperacion
