import React, { Component } from 'react'


class AgregarGolondrina extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nombre: '',
      energia: 0,
    }
  }

  setValue(event) {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  render() {
    return (
      <div className='row mt-4'>
        <div className='col-10'>
          <div className='card text-white bg-primary'>
            <div className='card-header'>
              <span className='lead'>agregar golondrina</span>
            </div>
            <div className='card-body text-dark bg-light'>
              <div className='form-group'>
                <label className='form-label mr-1'>
                  nombre:
                  <input className='form-control'
                    type='text'
                    name='nombre'
                    onChange={(event) => this.setValue(event)}
                  />
                </label>

                <label className='form-label mr-1'>
                  energía inicial: 
                  <input className='form-control'
                    type='number'
                    name='energia'
                    onChange={(event) => this.setValue(event)}
                  />
                </label>

                <button className='btn btn-primary mr-1'
                  onClick={() => this.aceptar()}
                >
                  confirmar
                </button>
                
                <button className='btn btn-secondary mr-1'
                  onClick={() => this.cancelar()}
                >
                  cancelar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  
  aceptar() {
    this.props.mainComponent.agregarGolondrina(this.state.nombre, this.state.energia)
    this.props.mainComponent.hideAgregarGolondrina()
  }

  cancelar() {
    this.props.mainComponent.hideAgregarGolondrina()
  }
}

export default AgregarGolondrina
