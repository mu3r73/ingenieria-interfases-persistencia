import React, { Component } from 'react'

import Golondrina from './Golondrina'


class Golondrinas extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-10'>
          <div className='card'>
            <div className='card-header'>
              <span className='lead'>golondrinas</span>
              {this.renderButtonAgregarGolondrina()}
            </div>
            <div className='card-body'>
              <ul>
                {
                  this.props.mainComponent.golondrinas().map(g =>
                    <Golondrina key={g.nombre()}
                      golondrina={g}
                      mainComponent={this.props.mainComponent}
                    />
                  )
                }
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderButtonAgregarGolondrina() {
    return (
      <button className='btn btn-primary float-right'
        onClick={() => this.props.mainComponent.showAgregarGolondrina()}
      >
        agregar golondrina
      </button>
    )
  }
}


export default Golondrinas
