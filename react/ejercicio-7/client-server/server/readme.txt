para instalar los módulos necesarios:
npm install

para correr el servidor:
npm start

dirección base:
http://localhost:4040

para que el servidor incluya la página react desde client/build:
1. editar server/src/server.js, descomentando la línea indicada en el código
2. en client/, correr: npm run build (esperar que termine)
3. en server/, correr: npm start

api:
/api/aviones, GET -> lista de aviones
/api/vuelos, GET -> lista de vuelos
/api/avion/<nombre>, GET -> datos del avión con ese nombre
/api/vuelosDeAvion/<nombre_avion>, GET -> vuelos del avión con ese nombre
