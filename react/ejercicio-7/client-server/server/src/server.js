const express = require('express')
const path = require('path')
const cors = require('cors')
const {IpFilter} = require('express-ipfilter')


const app = express()
const port = 4040

// ips en 'lista negra', a los que no les permitiremos acceder al sitio
const blacklist = []

const store = require('./apiDataObjects')

// ojo: el orden del middleware es importante

// bloquear acceso a hosts en 'lista negra'
//app.use(IpFilter(blacklist))

// mostrar errores
/*
app.use((err, req, res, next) => {
  const code = err.status || 500
  const msg = `${code}: ${err.message}`
  console.log(msg)
  res.status(code).send(msg)
})
*/

// servir la página de react desde client/build
//app.use(express.static(path.join(__dirname, '../../client/build')))

// permitir cors
app.use(cors({
  origin: 'http://localhost:3030'
}))
require('./routes')(app, store.store)


app.listen(port, () => {
  console.log(`server started @ port: ${port}`)
})
