const dominio = require('./dominio')


// clases para el api
// (alternativa: ensuciar el modelo(?) agregándole funciones asDataObject())

class AvionDataObject {
  constructor(avion) {
    this.nombre = avion.nombre()
    this.cantVuelos = avion.cantidadVuelosHechos()
    this.capacidad = avion.cantidadAsientos()
    this.pasajerosTransportados = avion.cantidadTotalPasajeros()
    this.porcentajeOcupacion = avion.porcentajeOcupacion()
    this.alturaCabina = avion.alturaCabina()
  }
}

class VueloDataObject {
  constructor(vuelo) {
    this.numero = vuelo.numero()
    this.tipo = vuelo.tipoAsString()
    this.origen = vuelo.origen().nombre()
    this.destino = vuelo.destino().nombre()
    this.avion = vuelo.avion().nombre()
    this.asientosLibres = vuelo.cantidadAsientosLibres()
    this.precioPasaje = vuelo.precioPasaje()
    this.politicaVenta = vuelo.politicaPrecio()
    this.pasajesVendidos = vuelo.cantidadPasajesEmitidos()
    this.importeTotalVendido = vuelo.importeTotalPasajesEmitidos()
  }
}

class CiudadDataObject {
  constructor(ciudad) {
    this.nombre = ciudad.nombre()
    this.pasajerosQueSalieron = ciudad.pasajerosQueSalieron()
    this.pasajerosQueLlegaron = ciudad.pasajerosQueLlegaron()
  }
}

class VueloStoreAsDataObjects {
  constructor(vueloStore) {
    this._vueloStore = vueloStore
  }

  vuelos() {
    return this.mappearAVueloDataObjects(this._vueloStore.vuelos())
  }
  
  vuelosDeAvionConNombre(nombre) {
    return this.mappearAVueloDataObjects(this._vueloStore.vuelosDeAvion(this._vueloStore.avionConNombre(nombre)))
  }
  
  mappearAVueloDataObjects(vuelos) {
    return vuelos.map(
      vuelo => new VueloDataObject(vuelo)
    )
  }

  aviones() {
    return this._vueloStore.aviones().map(
      avion => new AvionDataObject(avion)
    )
  }
  
  avionConNombre(nombre) {
    return new AvionDataObject(this._vueloStore.avionConNombre(nombre))
  }

  ciudades() {
    return this._vueloStore.ciudades().map(
      ciudad => new CiudadDataObject(ciudad)
    )
  }

  ciudadConNombre(nombre) {
    return new CiudadDataObject(this._vueloStore.ciudadConNombre(nombre))
  }

}

module.exports.store = new VueloStoreAsDataObjects(dominio.elStore)
