const util = require('util')


module.exports = (app, store) => {
  app.get(
    '/api/aviones',
    (req, res) => {
      res.send(store.aviones())
    }
  )

  app.get(
    '/api/avion/:nombre',
    (req, res) => {
      res.send(store.avionConNombre(req.params.nombre))
    }
  )

  app.get(
    '/api/vuelos',
    (req, res) => {
      res.send(store.vuelos())
    }
  )

  app.get(
    '/api/vuelosDeAvion/:nombreAvion',
    (req, res) => {
      res.send(store.vuelosDeAvionConNombre(req.params.nombreAvion))
    }
  )

  app.get(
    '/api/ciudades',
    (req, res) => {
      res.send(store.ciudades())
    }
  )

  app.get(
    '/api/ciudad/:nombre',
    (req, res) => {
      res.send(store.ciudadConNombre(req.params.nombre))
    }
  )

}
