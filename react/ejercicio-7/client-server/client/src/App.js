import React, { Component } from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'

import './App.css'

import InfoVuelosAviones from './components/InfoVuelosAviones'


class App extends Component {
  render() {
    return (
      <div>
      <InfoVuelosAviones />
      </div>
    )
  }
}

export default App
