import axios from 'axios'


const apiBaseUrl = '/api'

export async function fetchDatosAvion(nombre) {
  const res = await axios.get(`${apiBaseUrl}/avion/${nombre}`)
  return res.data
}

export async function fetchDatosVuelos() {
  const res = await axios.get(`${apiBaseUrl}/vuelos`)
  return res.data
}

export async function fetchDatosCiudad(nombre) {
  const res = await axios.get(`${apiBaseUrl}/ciudad/${nombre}`)
  return res.data
}
