import React, { Component } from 'react'

import { fetchDatosVuelos } from '../api-queries/fetchStuff'


class TablaVuelos extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vuelos: [],
    }
  }

  componentDidMount() {
    this.cargarVuelos()
  }

  cargarVuelos() {
    fetchDatosVuelos()
      .then((res) => {
        this.setState({
          vuelos: res
        })
      })
  }

  render() {
    return (
      <div className='panel panel-success mt-50'>
        <div className='panel-heading'>
          <h4>Vuelos (de todos los aviones)</h4>
        </div>
        <div className='panel-body'>
          <table className='table table-striped'>
            <thead>
              <tr>
                {this.renderTitulos()}
              </tr>
            </thead>
            <tbody>
              {this.renderVuelos()}
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  mainComponent() {
    return this.props.mainComponent
  }

  vuelos() {
    return this.state.vuelos
  }

  renderTitulos() {
    const titulos = ["Tipo de vuelo", "Origen", "Destino", "Avión",
                     "Asientos libres", "Precio pasaje", "Política de venta",
                     "Pasajes vendidos", "Importe total vendido"]
    return titulos.map(
      titulo => <th key={titulo}>{titulo}</th>
    )
  }

  renderVuelos() {
    return this.vuelos().map(
      vuelo => 
        <DatosVuelo
          vuelo={vuelo}
          key={vuelo.numero}
          mainComponent={this.mainComponent()}
        />
    )
  }
}


class DatosVuelo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vuelo: props.vuelo,
    }
  }

  render() {
    const vuelo = this.vuelo()
    return (
      <tr>
        <td>{vuelo.tipo}</td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirCiudad(vuelo.origen)}
          >
            {vuelo.origen}
          </a>
        </td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirCiudad(vuelo.destino)}
          >
            {vuelo.destino}
          </a>
        </td>
        <td>
          <a
            role='button'
            onClick={() => this.elegirAvion(vuelo.avion)}
          >
            {vuelo.avion}
          </a>
        </td>
        <td>{vuelo.asientosLibres}</td>
        <td>{vuelo.precioPasaje}</td>
        <td>{vuelo.politicaVenta}</td>
        <td>{vuelo.pasajesVendidos}</td>
        <td>{vuelo.importeTotalVendido}</td>
      </tr>
    )
  }

  vuelo() {
    return this.state.vuelo
  }

  elegirAvion(nombre) {
    this.props.mainComponent.elegirAvion(nombre)
  }

  elegirCiudad(nombre) {
    this.props.mainComponent.elegirCiudad(nombre)
  }  
}

export default TablaVuelos
