/*
base:
https://github.com/obj2-material/javascript-dom/blob/master/react/ventas-aereas/aviones-vuelos-design-2.js
*/
import React, { Component } from 'react'

import DatosDelAvionElegido from './DatosDelAvionElegido'
import DatosDeLaCiudadElegida from './DatosDeLaCiudadElegida'
import TablaVuelos from './TablaVuelos'
import { fetchDatosAvion, fetchDatosCiudad } from '../api-queries/fetchStuff'


class InfoVuelosAviones extends Component {
  constructor(props) {
    super(props)
    this.state = {
      avionElegido: {},
      ciudadElegida: {},
    }
  }

  componentDidMount() {
    this.cargarAvionElegido('Boeing 737')
    this.cargarCiudadElegida('Buenos Aires')
  }

  cargarAvionElegido(nombre) {
    fetchDatosAvion(nombre)
      .then((res) => {
        this.setState({
          avionElegido: res,
        })
      })
  }

  cargarCiudadElegida(nombre) {
    fetchDatosCiudad(nombre)
      .then((res) => {
        this.setState({
          ciudadElegida: res,
        })
      })
  }

  render() {
    return (
      <div className='container ml-20 mr-20'>
        <TablaVuelos
          mainComponent={this}
        />
        <DatosDelAvionElegido
          mainComponent={this}
        />
        <DatosDeLaCiudadElegida
          mainComponent={this}
        />
      </div>
    )
  }

  avionElegido() {
    return this.state.avionElegido
  }

  elegirAvion(nombre) {
    this.cargarAvionElegido(nombre)
  }

  ciudadElegida() {
    return this.state.ciudadElegida
  }

  elegirCiudad(nombre) {
    this.cargarCiudadElegida(nombre)
  }

}

export default InfoVuelosAviones
