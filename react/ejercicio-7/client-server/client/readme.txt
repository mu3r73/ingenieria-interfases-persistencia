This project was bootstrapped with create-react-app
(https://github.com/facebookincubator/create-react-app).


---

para instalar los módulos necesarios:
npm install

para correr el servidor:
npm start

(aclaración: como es para probar cosas y jugar,
corre un servidor del lado del 'cliente',
que recompila automáticamente cuando cambiamos algo,
para no tener que hacerlo a mano)

dirección:
http://localhost:3030
