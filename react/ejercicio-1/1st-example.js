/*
base:
https://github.com/obj2-material/javascript-dom/blob/master/react/iniciales/first-example.js
*/
const React = require('react')
const ReactDOM = require('react-dom')


class FirstExample extends React.Component {
  constructor(props) {
    super(props);
    // el state incluye los aspectos dinámicos de la página
    this.state = {
      texto: "texto original",
      tamanioFuente: "medium",
      colorFuente: "black",
      textoBoton: "react magic",
    }
  }

  render() {
    const theStyle = {
      fontSize: this.state.tamanioFuente,
      color: this.state.colorFuente,
    }
    // Los elementos dinámicos no necesitan id, lo que es dinámico
    // se expresa en JavaScript, es lo que está entre llaves.
    return (
      <div>
        <p>
          <span style={ theStyle }>
            { this.state.texto }
          </span>
        </p>
        <button onClick={() => this.changeTextAndFont()}>
          { this.state.textoBoton }
        </button>
        <button onClick={() => this.changeFontColor()}>
          cambiar a color { this.getTraduccionColor(this.getProxColor(this.state.colorFuente)) }
        </button>
      </div>
    )
  }

  // función que reacciona al evento de apretar el botón
  changeTextAndFont() {
    // al cambiar el state *usando setState*,
    // React se encarga solito de actualizar la pantalla
    this.setState({
      texto: "hola react",
      tamanioFuente: "25px",
      textoBoton: "ya está",
    })
  }

  // función que cambia el color del texto
  changeFontColor() {
    this.setState({
      colorFuente: this.getProxColor(this.state.colorFuente),
    })
  }

  getProxColor(color) {
    const proxColor = {
      "black": "red",
      "red": "blue",
      "blue": "green",
      "green": "brown",
      "brown": "black",
    }
    return proxColor[color]
  }

  getTraduccionColor(color) {
    const traducirColor = {
      "black": "negro",
      "red": "rojo",
      "blue": "azul",
      "green": "verde",
      "brown": "marrón",
    }
    return traducirColor[color]
  }
}

ReactDOM.render(
  <FirstExample />,
  document.getElementById('reactPage')
);
