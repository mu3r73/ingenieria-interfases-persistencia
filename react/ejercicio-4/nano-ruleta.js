import React, { Component } from 'react'
import ReactDOM from 'react-dom'


class NanoRuleta extends Component {
  constructor() {
    super()
    this.state = {
      jugador: {
        saldo: 123,
      },
      apuesta: {
        color: '',
        monto: 0,
      },
      error: '',
    }
  }

  getSaldoJugador() {
    return this.state.jugador.saldo
  }

  getColorApostado() {
    return this.state.apuesta.color
  }

  getMontoApostado() {
    return this.state.apuesta.monto
  }

  getError() {
    return this.state.error
  }

  apostar(color, monto) {
    if (this.getSaldoJugador() < monto) {
      this.setState({
        error: `el jugador sólo tiene $${this.getSaldoJugador()}, no puede apostar $${monto}`,
      })
      return
    }
    this.setState({
      jugador: {
        saldo: this.getSaldoJugador() - monto,
      },
      apuesta: {
        color,
        monto: this.getMontoApostado() + monto,
      },
      error: '',
    })
  }

  salio(color) {
    const gano = (color === this.getColorApostado())
                ? this.getMontoApostado() * 2
                : 0
    this.setState({
      jugador: {
        saldo: this.getSaldoJugador() + gano,
      },
      apuesta: {
        color: '',
        monto: 0,
      },
      error: '',
    })
  }

  render() {
    console.log(this.state)
    return (
      <div>
        <Saldo mainComponent={this} />
        {this.renderBotonesApuesta()}
        {this.state.error && <Error mainComponent={this} /> }
        {this.state.apuesta.color && <ApuestaActual mainComponent={this} /> }
        <hr />
        {this.renderBotonesGano()}
      </div>
    )
  }

  renderBotonesApuesta() {
    const botones = [
      {color: 'negro', monto: 10},
      {color: 'negro', monto: 50},
      {color: 'rojo', monto: 10},
      {color: 'rojo', monto: 50},
    ]
    return (
      <div>
        {
          botones.map(boton =>
            <BotonApuesta
              mainComponent={this}
              color={boton.color}
              monto={boton.monto}
              key={`${boton.color}${boton.monto}`}
            />
          )
        }
      </div>
    )
  }

  renderBotonesGano() {
    const colores = ['rojo', 'negro', '0']
    return (
      <div>
        {
          colores.map(col =>
            <BotonGano mainComponent={this} color={col} key={col} />
          )
        }
      </div>
    )
  }
}

class Saldo extends Component {
  render() {
    return (
      <div className='text-primary'>
        <p>saldo del jugador: {this.props.mainComponent.getSaldoJugador()}</p>
      </div>
    )
  }
}

class BotonApuesta extends Component {
  render() {
    const colorApostado = this.props.mainComponent.getColorApostado()
    return (
      <button
        disabled={(colorApostado && colorApostado !== this.props.color)}
        className={`btn ${this.props.color==='negro' ? 'btn-dark' : 'btn-danger'} mr-1`}
        onClick={() => this.props.mainComponent.apostar(this.props.color, this.props.monto)}
      >
        apostar {this.props.monto} a {this.props.color}
      </button>
    )
  }
}

class Error extends Component {
  render() {
    return (
      <div className='text-danger'>
        <small>{this.props.mainComponent.getError()}</small>
      </div>
    )
  }
}

class ApuestaActual extends Component {
  render() {
    return (
      <div className='text-info mt-4'>
        <p>
          apuesta actual: {this.props.mainComponent.getMontoApostado()} al {this.props.mainComponent.getColorApostado()}
        </p>
      </div>
    )
  }
}

class BotonGano extends Component {
  render() {
    return (
      <button
        className='btn mr-1'
        onClick={() => this.props.mainComponent.salio(this.props.color)}
      >
        salió {this.props.color}
      </button>      
    )
  }
}


ReactDOM.render(
  <NanoRuleta />,
  document.getElementById('reactPage')
)
