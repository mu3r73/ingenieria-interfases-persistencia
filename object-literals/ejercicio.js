// considerar listas de registros de lluvia, con año, ciudad y milímetros caídos

let centroBuenosAires = [
  {year: 1902, city: 'Chas', mm: 822},
  {year: 1903, city: 'Chas', mm: 901},
  {year: 1904, city: 'Chas', mm: 940},
  {year: 1902, city: 'Newton', mm: 749},
  {year: 1903, city: 'Newton', mm: 748},
  {year: 1903, city: 'Villanueva', mm: 951},
  {year: 1905, city: 'Villanueva', mm: 922},
  {year: 1902, city: 'Gral. Belgrano', mm: 883}
]

let misiones = [
  {year: 1902, city: 'Oberá', mm: 2304},
  {year: 1903, city: 'Oberá', mm: 1891},
  {year: 1902, city: 'Andresito', mm: 1504}
]

// Armar funciones que permitan, para una lista de registros de lluvia

// cuánto llovió en un año en una ciudad
function cuantoLlovioEn(rs, ciudad, anho) {
  return rs.reduce(
    (res, reg) => {
      if ((reg.city === ciudad) && (reg.year === anho)) {
        res += reg.mm
      }
      return res
    },
    0
  )
}

// si hay al menos un registro de una ciudad
function hayDatosDe(rs, ciudad) {
  return rs.some(
    reg => reg.city === ciudad
  )
}

// los registros que correspondan a una cantidad de milímetros llovidos mayor a un número dado
function registrosQueExceden(rs, mm) {
  return rs.filter(
    reg => reg.mm > mm
  )
}

// cuánto llovió en total en una ciudad, sumando todos los años para los que hay registro
function totalLluvias(rs, ciudad) {
  return rs.reduce(
    (res, reg) => {
      if (reg.city === ciudad) {
        res += reg.mm
      }
      return res
    },
    0
  )
}

// si una ciudad está bien regada, o sea, tiene al menos dos registros,
// y en cada uno llovió al menos 900 mm
function estaBienRegada(rs, ciudad) {
  var rsc = rs.filter(
    reg => reg.city === ciudad
  )
  return rsc.every(
    reg => reg.mm >= 900
  ) && rsc.length >= 2
}

// el registro (con todos los datos) con la mayor cantidad de lluvia
function regConMaxLluvia(rs) {
  return rs.reduce(
    (res, reg) => {
      if (reg.mm > res.mm) {
        res = reg
      }
      return res
    },
    {mm: 0}
  )
}

// para qué años hay registro en una ciudad.
function anhosConAlgunRegistro(rs) {
  return rs.reduce(
    (res, reg) => {
      if (!res.includes(reg.year)) {
        res.push(reg.year)
      }
      return res
    },
    []
  )
}

// los registros de un año con ciudad y mm
function registrosDelAnho(rs, anho) {
  return rs.reduce(
    (res, reg) => {
      if (reg.year === anho) {
        res.push({city: reg.city, mm: reg.mm})
      }
      return res
    },
    []
  )
}

// (difícil) el resultado de agregar registros de una ciudad con este formato
// agregarRegistros(centroBuenosAires, "Ranchos", [{year: 1902, mm: 1041}, {year: 1903, mm: 1054}])
// Tip: ver Object.assign
function agregarRegistros(rs, ciudad, regs) {
  for (reg of regs) {
    rs.push({year: reg.year, city: ciudad, mm: reg.mm})
  }
}

function agregarRegistrosDestructuring(rs, ciudad, regs) {
  for (reg of regs) {
    rs.push({city: ciudad, ...reg})
  }
}

function agregarRegistrosReduce(rs, ciudad, regs) {
  return regs.reduce(
    (res, reg) => {
      res.push(Object.assign({city: ciudad}, reg))
      return res
    },
    rs
  )
}

// (aún más difícil) lo mismo pero con este formato
// agregarRegistros(centroBuenosAires, "Ranchos", 1902, 1041, 1903, 1054)
// (alterna año y mm)
// ver cantidad variable de argumentos
function agregarRegistrosV2(rs, ciudad, ... regs) {
  return regs.reduce(
    (res, _, idx) => {
      if (idx % 2 === 0) {
        res.push({year: regs[idx], city: ciudad, mm: regs[idx + 1]})
      }
      return res
    },
    rs
  )
}

// una variante más fácil es
// agregarRegistros(centroBuenosAires, "Ranchos", [1902, 1041, 1903, 1054])
function agregarRegistrosV3(rs, ciudad, regs) {
  return regs.reduce(
    (res, _, idx) => {
      if (idx % 2 === 0) {
        res.push({year: regs[idx], city: ciudad, mm: regs[idx + 1]})
      }
      return res
    },
    rs
  )
}
