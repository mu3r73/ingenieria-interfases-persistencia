// traducido de wollok
// TODO: probar

export class EstacionDeServicio {
  constructor() {
    this.dispositivos = []
    this.animalesAtendidos = []
  }

  instalarDispositivo(dispositivo) {
		this.dispositivos.push(dispositivo)
	}

	desinstalarDispositivo(dispositivo) {
		this.dispositivos.splice(this.dispositivos.indexOf(dispositivo), 1)
	}

  puedeAtender(animal) {
		this.dispositivos.some(
			dispositivo => dispositivo.puedeAtenderA(animal)
		)
	}

	atender(animal) {
		this.dispositivos.find(
			dispositivo => dispositivo.puedeAtenderA(animal)
		).atender(animal)
    this.animalesAtendidos.add(animal)
	}

	recargarDispositivos() {
		this.dispositivos.filter(
			dispositivo => dispositivo.necesitaRecarga
		).forEach(
			dispositivo => dispositivo.recargar()
		)
	}

	fueAtendido(animal) {
		return this.animalesAtendidos.includes(animal)
	}

	atendidosQueConvieneVacunar() {
		return this.animalesAtendidos.filter(
			animal => animal.convieneVacunar
		)
	}

	atendidoMasPesado() {
    return this.animalesAtendidos.reduce(
      (max, curr) => (curr.peso > max.peso) ? curr : max
    )
	}

	pesoTotalDeAtendidos() {
		return animalesAtendidos.reduce(
			(tot, curr) => tot += curr.peso,
      0
		)
	}
}


module.exports = {
  EstacionDeServicio
}
