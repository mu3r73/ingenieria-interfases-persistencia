Implementar las clases necesarias para resolver los ejercicios que se indican de las guías de Objetos 1

Guía 5 ejercicio 4: atención de animales.
Guía 7 ejercicio 1: agregados a atención de animales.



Atención de animales

En un campo hay que atender a los animales, que tienen varias necesidades.
Consideremos vacas, gallinas y cerdos, que tienen estas carácterísticas.

Vaca
* Cuando come aumenta el peso en lo que comió / 3 y le da sed.
* Cuando bebe se le va la sed y pierde 500 g de peso.
* Conviene vacunarla una vez, o sea, si no se la vacunó conviene vacunarla, y si ya se la vacunó no conviene volverla a vacunar.
* Tiene hambre si pesa menos de 200 kg.
* Cada tanto se la lleva a caminar, en cada caminata pierde 3 kg.

Cerdo

* Cuando come aumenta el peso en lo que comió - 200 g (si come menos de 200 g no aumenta nada); si come más de 1 kg se le va el hambre, si no no.
* Quiero saber, para cada cerdo, cuánto comió la vez que más comió.
* Siempre conviene vacunarlo.
* Cuando bebe se le va la sed, le da hambre, y pierde 1 kg de peso.
* Si come más de tres veces sin beber le da sed.

Gallina

* Cuando come no se observa ningún cambio, siempre pesa 4 kg.
* Siempre tiene hambre, nunca tiene sed, nunca conviene vacunarla.
* Quiero saber, para una gallina, cuántas veces fue a comer.

Como se ve, importa cuánto come un animal cuando come (excepto para las gallinas), pero no cuánto bebe cuando bebe.

Hay varios dispositivos de atención automática a los animales:

1. Comederos normales: cada comedero da de comer una ración, que es una cantidad fija que varía para cada comedero. Puede atender a los animales con hambre que pesen menos de lo que soporta el comedero, que también es un valor que depende del comedero. Un comedero normal necesita recarga si le quedan menos de 10 raciones, cuando se lo recarga se le cargan 30 raciones.

2. Comederos inteligente: le dan de comer a un animal su peso / 100. Pueden atender a cualquier animal con hambre. Un comedero inteligente necesita recarga si le quedan menos de 15 kg, al recargarlo se lo lleva hasta su capacidad máxima (que se indica para cada comedero).

3. Bebederos: dan de beber a un animal, pueden atender a los animales con sed. Un bebedero necesita recarga cada 20 animales que atiende, lo que se le hace al recargarlo no se registra en el sistema (sí que se lo recarga para volver a contar desde ahí 20 animales atendidos).

4. Vacunatorios: vacunan a un animal, pueden atender a los animales que conviene vacunar. Un vacunatorio necesita recarga si se queda sin vacunas, al atenderlo se le recargan 50 vacunas.

Una estación de servicio tiene una cantidad indeterminada dispositivos de atención automática.

Se pide:

a) Modelar lo que se describió de forma tal de poder

* saber si un animal puede ser atendido por una estación de servicio; o sea, si se lo puede atender en alguno de sus dispositivos, mediante una pregunta a un objeto. ¿A cuál objeto?

* indicar que un animal se atiende en una estación, en este caso se elige un dispositivo al azar que pueda atenderlo, y se lleva al animal a ese dispositivo para que lo atienda. Esto mediante una orden a un objeto.

* recargar los dispositivos que necesitan recarga en una estación de servicio, envíandole un mensaje a la estación de servicio.

* saber para una estación de servicio: si un animal fue o no atendido, el conjunto de los animales atendidos en esa estación que conviene vacunar, el animal más pesado que atendió, y el peso total de los animales atendidos.

b) Armar un test con una vaca, un cerdo y una gallina, y un dispositivo de cada tipo. Hacer al menos 5 asserts sobre si un animal se puede o no atender en un dispositivo, tiene que haber al menos dos que sí y dos que no.

c) Armar un segundo test donde haya dos vacas, dos cerdos y una gallina. Armar dos estaciones de servicios, una con dos comederos normales, que soporten animales de hasta 70 y 110 kg respectivamente, y un bebedero, el otro con un comedero inteligente y un vacunatorio. Tiene que pasar que

* haya un animal que pueda atenderse en la primer estación y no en la segunda,

* haya un animal que pueda atenderse en la segunda estación y no en la primera,

* uno de los comederos necesite recarga,

* uno de los animales pueda atenderse en las dos estaciones, en cada una en un solo dispositivo. P.ej. una vaca de 120 kg, ya vacunada y con sed.

Hacer estos asserts, después hacer que el animal que puede atenderse en las dos estaciones se atienda en ambas, probar que quede como tiene que quedar. Mandar otros dos animales a atenderse, después hacer asserts sobre lo que se pide de las estaciones de servicio. Hacer el grafo de objetos correspondiente al final de este segundo test.


Agregados

Parte 1: Variantes de animales

A partir de los animales conocidos, o sea vacas, cerdos y gallinas, agregar estas variantes.

* Cerdo resistente: Desde que lo vacunan no conviene vacunarlo. Ahora, si después de recibir una vacuna come de una vez, 5 kg o más, ahí conviene vacunarlo de nuevo.

* Vaca zen: Una vez que se la vacuna, nunca más tiene sed.

* Gallina turuleca: Además de comer, nos interesa cuando pone huevos. Cada vez que pone huevos, pueden ser uno, dos o tres. Implementar esto como tres mensajes distintos, haPuestoUnHuevo(), haPuestoDosHuevos(), haPuestoTresHuevos(). Si el total de huevos que puso desde que nació es impar, entonces tiene sed, si es par, entonces no tiene sed.

Parte 2: Felicidad

Agregar la capacidad, para cualquier animal, de saber si está feliz o no. Para que un animal esté feliz, tiene que pasar que no convenga vacunarlo, y además, que pese o bien menos de 10 kg, o bien más de 150 kg. Agregar también estas tres variantes nuevas de animales:

* Vaca reflexiva: para estar feliz, además, tuvo que haber salido a caminar al menos 2 veces. Para las vacas reflexivas (¡OJO! no para todas) hay que contar cuántas veces salió a caminar.

* Vaca filósofa: es como la reflexiva, salvo que para estar feliz, además de todas las condiciones que trae por ser reflexiva, tiene que no tener sed.

* Cerdito alegre: es como el cerdo normal (o sea, no como el cerdo resistente), y siempre está feliz. No hay ningún cerdo que sea ser feliz y también resistente.
