// traducido de wollok
// TODO: probar

class Dispositivo {
  puedeAtenderA(animal) {
		throw new Error("Dispositivo.puedeAtenderA: abstracto")
	}

	get estaVacio() {
    throw new Error("Dispositivo.estaVacio: abstracto")
	}

	atender(animal) {
		if (this.estaVacio) {
			throw new Error(this.mensajeVacio)
		}
	}

  get mensajeVacio() {
    throw new Error("Dispositivo.mensajeVacio: abstracto")
  }

	get necesitaRecarga() {
		throw new Error("Dispositivo.necesitaRecarga: abstracto")
	}

	recargar() {
		throw new Error("Dispositivo.recargar: abstracto")
	}
}


class ComederoNormal extends Dispositivo {
	constructor(pesoRacion, pesoMaximoPorAnimal) {
		this._pesoRacion = pesoRacion
		this._pesoMaximoPorAnimal = pesoMaximoPorAnimal
    this.cantRaciones = 0
	}

  get pesoRacion() {
    return this._pesoRacion
  }

  set pesoRacion(num) {
    throw new Error("ComederoNormal.pesoRacion no es setteable")
  }

  get pesoMaximoPorAnimal() {
    return this._pesoMaximoPorAnimal
  }

  set pesoMaximoPorAnimal(num) {
    throw new Error("ComederoNormal.pesoMaximoPorAnimal no es setteable")
  }

	puedeAtenderA(animal) {
		return animal.tieneHambre
			&& animal.peso <= this.pesoMaximoPorAnimal
	}

	get estaVacio() {
		return this.cantRaciones == 0
	}

	atender(animal) {
		super.atender(animal)
		animal.comer(this.pesoRacion)
		this.cantRaciones--
	}

  get mensajeVacio() {
    return "ComederoNormal.atender: no queda alimento"
  }

	get necesitaRecarga() {
		return this.cantRaciones < 10
	}

	recargar() {
		cantRaciones += 30
	}
}


class ComederoInteligente extends Dispositivo {
	constructor(capacidadMaxima) {
		this._capacidadMaxima = capacidadMaxima
    this.kgAlimento = 0	// kg
	}

  get capacidadMaxima() {
    return this._capacidadMaxima
  }

  set capacidadMaxima(num) {
    throw new Error("ComederoInteligente.capacidadMaxima no es setteable")
  }

  get kgAlimento() {
    return this._kgAlimento
  }

  set kgAlimento(num) {
    this._kgAlimento = num
  }

	puedeAtenderA(animal) {
		return animal.tieneHambre
	}

	get estaVacio() {
		return this.kgAlimento == 0
	}

	atender(animal) {
		super.atender(animal)
    let kgComidos = Math.min(this.kgAlimento, 1000 * animal.peso / 100)
		animal.comer(kgComidos)
		this.kgAlimento -= kgComidos
	}

  get mensajeVacio() {
    return "ComederoInteligente.atender: no queda alimento"
  }

	get necesitaRecarga() {
		return this.kgAlimento < 15
	}

	recargar() {
		this.kgAlimento = this.capacidadMaxima
	}
}


class Bebedero {
  constructor() {
    this.animalesAtendidos = 0
  }

  get animalesAtendidos() {
    return this._animalesAtendidos
  }

  set animalesAtendidos(num) {
    this._animalesAtendidos = num
  }

	puedeAtenderA(animal) {
		return animal.tieneSed
	}

	get estaVacio() {
		return this.animalesAtendidos == 20
	}

	atender(animal) {
		super.atender(animal)
		animal.beber()
		this.animalesAtendidos++
	}

  get mensajeVacio() {
    return "Bebedero.atender: no queda bebida"
  }

	get necesitaRecarga() {
		return this.estaVacio
	}

	recargar() {
		this.animalesAtendidos = 0
	}
}


class Vacunatorio {
  constructor() {
    this.vacunasDisponibles = 0
  }

  get vacunasDisponibles() {
    return this._vacunasDisponibles
  }

  set vacunasDisponibles(num) {
    this._vacunasDisponibles = num
  }

	puedeAtenderA(animal) {
		return animal.convieneVacunar
	}

	get estaVacio() {
		return this.vacunasDisponibles == 0
	}

	atender(animal) {
		super.atender(animal)
		animal.recibirVacuna()
		this.vacunasDisponibles--
	}

  get mensajeVacio() {
    return "Vacunatorio.atender: no quedan vacunas"
  }

	get necesitaRecarga() {
		return this.vacunasDisponibles == 0
	}

	recargar() {
		this.vacunasDisponibles = 50
	}
}


module.exports = {
  Dispositivo,
  ComederoNormal,
  ComederoInteligente,
  Bebedero,
  Vacunatorio,  
}
