// traducido de wollok
// TODO: probar

class Animal {
  constructor() {
    this.peso = undefined // en kg
    this.tieneHambre = undefined // bool
    this.tieneSed = undefined // bool
  }

	get peso() {
    return this._peso
  }

  set peso(num) {
    this._peso = num
  }

  get tieneHambre() {
    return this._tieneHambre
  }

  set tieneHambre(bool) {
    this._tieneHambre = bool
  }

  get tieneSed() {
    return this._tieneSed
  }

  set tieneSed(bool) {
    this._tieneSed = bool
  }

	get estaFeliz() {
		return this.convieneVacunar()
			and (this.peso < 10 or this.peso > 150)
	}
}


class Vaca extends Animal {
  constructor(peso) {
    super()
    this.peso = peso
    this.tieneSed = false
    this.fueVacunada = false
	}

  get tieneHambre() {
		return this.peso < 200
	}

  set tieneHambre(bool) {
    throw new Error("Vaca.tieneHambre no es setteable")
  }

  get fueVacunada() {
    return this._fueVacunada
  }

  set fueVacunada(bool) {
    this._fueVacunada = bool
  }

  get convieneVacunar() {
		return !this.fueVacunada
	}

	comer(gramos) {
		this.peso += gramos / (3 * 1000)	// gana gramos / 3
		this.tieneSed = true
	}

	beber() {
		this.tieneSed = false
		this.peso -= 500 / 1000	// pierde 500 gramos
	}

	recibirVacuna() {
		this.fueVacunada = true
	}

	caminar() {
		this.peso -= 3
	}
}


class VacaZen extends Vaca {
	get tieneSed() {
		return this.fueVacunada || super.tieneSed
	}
}


class VacaReflexiva extends Vaca {
	constructor(peso) {
    super(peso)
    this._vecesQueCamino = 0
  }

  get vecesQueCamino() {
    return this._vecesQueCamino
  }

  set vecesQueCamino(num) {
    this._vecesQueCamino = num
  }

	caminar() {
		this.vecesQueCamino++
	}

	get estaFeliz() {
		return super.estaFeliz && (this.vecesQueCamino >= 2)
	}
}


class VacaFilosofa extends VacaReflexiva {
	get estaFeliz() {
		return super.estaFeliz && !this.tieneSed
	}
}


class Cerdo extends Animal {
	constructor(peso) {
		super()
    this.peso = peso
    this.tieneSed = false
    this.tieneHambre = false
    this.maxComido = 0
    this.vecesQueComioSinBeber = 0
    this.convieneVacunar = true
	}

  get maxComido() {
    return this._maxComido
  }

  set maxComido(num) {
    this._maxComido = num
  }

  get vecesQueComioSinBeber() {
    return this._vecesQueComioSinBeber
  }

  set vecesQueComioSinBeber(num) {
    this._vecesQueComioSinBeber = num
  }

  get convieneVacunar() {
    return this._convieneVacunar
  }

  set convieneVacunar(bool) {
    this._convieneVacunar = bool
  }

	comer(gramos) {
		if (gramos > 200) {
			this.peso += (gramos - 200) / 1000
		}

    if (gramos > 1000) {
			this.tieneHambre = false
		}

    this.maxComido = Math.max(this.maxComido, gramos)

    this.vecesQueComioSinBeber++
		if (this.vecesQueComioSinBeber > 3) {
			this.tieneSed = true
		}
	}

	beber() {
		this.tieneSed = false
		this.tieneHambre = true
		this.peso--
		this.vecesQueComioSinBeber = 0
	}

	recibirVacuna() {
	}
}


class CerdoResistente extends Cerdo {
	recibirVacuna() {
		this.convieneVacunar = false
	}

	comer(gramos) {
		super.comer(gramos)
		if (gramos > 5000) {
			this.convieneVacunar = true
		}
	}
}


class CerditoAlegre extends Cerdo {
	get estaFeliz() {
		return true
	}
}


class Gallina extends Animal {
  constructor() {
    super()
    this._peso = 4
    this._tieneHambre = true
    this._tieneSed = false
    this._convieneVacunar = false
    this.vecesQueComio = 0
  }

  set peso(num) {
    throw new Error("Gallina.peso no es setteable")
  }

  set tieneHambre(bool) {
    throw new Error("Gallina.tieneHambre no es setteable")
  }

  set tieneSed(bool) {
    throw new Error("Gallina.tieneSed no es setteable")
  }

  set convieneVacunar(bool) {
    throw new Error("Gallina.convieneVacunar no es setteable")
  }

  get convieneVacunar() {
    return this._convieneVacunar
  }

  get vecesQueComio() {
    return this._vecesQueComio
  }

  set vecesQueComio(num) {
    this._vecesQueComio = num
  }

	comer(gramos) {
		vecesQueComio++
	}

	recibirVacuna() {
	}
}

class GallinaTuruleca extends Gallina {
  constructor() {
    super()
    this.huevosPuestos = 0
  }

  get huevosPuestos() {
    return this._huevosPuestos
  }

  set huevosPuestos(num) {
    this._huevosPuestos = num
  }

	haPuestoUnHuevo() {
		this.huevosPuestos++
	}

	haPuestoDosHuevos() {
		this.huevosPuestos += 2
	}

	haPuestoTresHuevos() {
		this.huevosPuestos += 3
	}

	get tieneSed() {
		return this.huevosPuestos % 2 == 1
	}
}


module.exports = {
  Animal,
  Vaca,
  VacaZen,
  VacaReflexiva,
  VacaFilosofa,
  Cerdo,
  CerdoResistente,
  CerditoAlegre,
  Gallina,
  GallinaTuruleca,
}
