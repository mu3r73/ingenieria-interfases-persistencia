// construir una función que,
// dados una lista de números ln y un número x,
// devuelva una lista con dos listas,
// la primera con los elementos de ln que sean números menores o iguales a x,
// la segunda con los que sean mayores a x
function separarSegun(ns, x) {
  var res = [[], []]
  for (n of ns) {
    if (n <= x) {
      res[0].push(n)
    } else {
      res[1].push(n)
    }
  }
  return res
}

function separarSegunReduce(ns, x) {
  return ns.reduce(
    (accumulator, current) => {
      if (current <= 2) {
        accumulator[0].push(current)
      } else {
        accumulator[1].push(current)
      }
      return accumulator
    },
    [[], []]
  )
}
