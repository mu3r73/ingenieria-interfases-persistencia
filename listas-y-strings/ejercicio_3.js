// construir una función que,
// dados una lista lln donde cada elemento es, a su vez, una lista de números,
// y un número x,
// devuelva una lista con las listas en lln donde está x
function listasDondeEsta(nns, x) {
  return nns.filter(
    ns => ns.includes(x)
  )
}


// construir la función repetir(str, n)
// que devuelve un String consistente en n copias del String str
function repetir(s, n) {
  return s.repeat(n)
}
