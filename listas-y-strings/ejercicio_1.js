const martin = [
  "Aquí", "me", "pongo", "a", "cantar",
  "al", "compás", "de", "la", "vigüela",
  "que", "al", "hombre", "que", "lo", "desvela",
  "una", "pena", "extraordinaria",
  "como", "el", "ave", "solitaria",
  "sólo", "al", "cantar", "se", "consuela"
]

const canterville = [
  "Yo", "era", "un", "hombre", "bueno",
  "si", "hay", "alguien", "bueno", "en", "este", "lugar",
  "pagué", "todas", "mis", "deudas",
  "perdí", "mi", "oportunidad", "de", "amar"
]

const comidas = [
  "puré", "milanesa", "empanada", "arroz", "fideos", "mayonesa"
]

const generos = [
  "rock", "pop", "disco", "rap"
]

// construir funciones que permitan obtener, para una lista de palabras:

// las que empiecen con “a” o con “A”
function empiezanConA(ps) {
  return empiezanCon(ps, 'a')
}

function empiezanCon(ps, l) {
  return ps.filter(
    p => p.toUpperCase().startsWith(l.toUpperCase())
  )
}

// las que tengan, al menos, una “a” o “A”
function tienenA(ps) {
  return tienen(ps, 'a')
}

function tienen(ps, l) {
  return ps.filter(
    p => p.toUpperCase().includes(l.toUpperCase())
  )
}

// las que tengan, al menos, dos “a” o “A”
function tienenAlMenosDosA(ps) {
  return tienenAlMenosDos(ps, 'a')
}

function tienenAlMenosDos(ps, l) {
  return ps.filter(
    p => contarOcurrencias(p.toUpperCase(), l.toUpperCase()) >= 2
  )
}

function contarOcurrencias(s, subs) {
  return s.split(subs).length - 1
}

// si hay al menos una palabra, de más de una letra, que empieza y termina con la misma letra
function hayCoincidenciaPrimeraUltima(ps) {
  return ps.filter(
    p => p.length > 1 && coincidenPrimeraUltima(p)
  )
}

function coincidenPrimeraUltima(p) {
  return p[0] === p[p.length - 1]
}

// la lista con las mismas palabras, todas al revés
function alReves(ps) {
  return ps.map(
    p => invertir(p)
  )
}

function invertir(p) {
  return p.split("").reverse().join("")
}

// la primera palabra de más de 7 letras
function primeraPalabraDeMasDe7Letras(ps) {
  return primeraPalabraDeMasDeNLetras(ps, 7)
}

function primeraPalabraDeMasDeNLetras(ps, n) {
  for (p of ps) {
    if (p.length > n) {
      return p
    }
  }
}
